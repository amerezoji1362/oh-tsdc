# Repo information

- The TsDC-DB (RDF) was built on [TsDC-DB.md](TsDC-DB.md)
- Please work directly in the RDF (master).
- The only purpose of the markdown output [filename & link] is visualization, e.g. in order to get feedback from contributors which are not familiar with RDF (and refuse learning to read it).
- We also accept final thesis', PhD's and internships ;)

## Generated content

Graph representations (logical and visual) of the RDF part of the standard
are generated after each commit to this repo,
and are stored [here](https://osegermany.gitlab.io/oh-tsdc/).