# TsDC

This documents contains Technology-specific Documentation Criteria (TsDC)
according to [DIN SPEC3105-1](https://gitlab.com/OSEGermany/OHS/-/blob/master/DIN_SPEC_3105-1.md)
in modular form. Each module bears an identifier (TsDC-ID) so requirements can be
unambiguously referenced. For that, this document is automatically exported into
machine-readable Linked Open Data (OWL, see the TTL file in this repository).

To make this possible, this document needs to follow the defined structure it already has.\
Please don't break it.

<!---

Note that ASM-PCB & ASM-GEN are now connected here, breaking the original logic in TsDC-print

--->

---

1. TsDC are formulated for 3 major domains:
   - component (`COM`)\
     = the parts your machine consists of
   - assembly (`ASM`)\
     = ensemble of joint components
   - post-processing (`POST`)\
    = anything done to components or assemblies after they've been built
2. TsDC can be:
   - `M` = mandatory
   - `T` = mandatory if necessary for the technical design
   - `B` = a recommended best practice
3. **This document only defines the common source files that should be linked in your metadata file.**
   - [ ] TODO: attach explanatory information & specific requirements (material specification etc.) to each item

## COM

### COM-MAN

- M
  - design
- T
  - manufacturing-instruction
  - calculation
- B
  - rationale

### COM-STD

- standard-designation

### COM-OSH

- manifest-file

### COM-BUY

- reference-buy

## ASM

### ASM-GEN

- M
  - bom
  - design
- T
  - calculation
  - list-auxiliary-components
  - list-special-tools
  - manufacturing-instruction
- B
  - rationale

### ASM-MEC

- T
  - calibration plan

### ASM-PCB

- M
  - schematic

### ASM-WELD

- T
  - welding-sequence-plan

## POST

<!---
POST-COT & POST-PROP are identical here → replace by POST-MAT?
--->

### POST-COT

- T
  - list-auxiliary-components
  - list-special-tools
  - processing-instruction
- B
  - rationale

### POST-PROP

- T
  - list-auxiliary-components
  - list-special-tools
  - processing-instruction
- B
  - rationale

### POST-IT

- M
  - reference-it
  - licence-it
- T
  - installation-instruction
- B
  - rationale
