---
# See `man pandoc | grep "Metadata variables"`
title: TsDC DB - print version
lang: en-US
charset: UTF-8
authors:
  - name: Martin Häuer
    affiliation: OSEG
    email: martin.haeuer@ose-germany.de
license: GPLv3
papersize: a4
geometry: "left=1cm,right=1cm,landscape"
output: pdf_document
---

|ID|MODULE|INFORMATION|COMMON SOURCE FILE|M|T|B|
|---|----|--------|------------|-|-|-|
|COM-MAN|manufactured component|geometry|CAD model, technical drawing|x|||
|||material properties|defined in CAD model, specified in technical drawing||x||
|||tolerances|defined in CAD model, specified in technical drawing||x||
|||surface specifications|defined in CAD model, specified in technical drawing||x||
|||manufacturing specifications (e.g.  manufacturing process, machine settings)|explanatory text document||x||
|||justification of technical design|explanatory text document, calculations|||x|
|||manufacturing instructions|explanatory text document|||x|
|||additional manufacturing files (e.g. STL files for additive manufacturing processes)||||x|
|COM-STD|standard component|standard designation|mention in bill of materials|x|||
|||justification of selection |explanatory text document|||x|
|COM-OSH|OSH component|unambiguous reference|link to documentation release|x|||
|||justification of selection |explanatory text document|||x|
|COM-PROP|proprietary component|unambiguous reference (e.g. specifications, interfaces, characteristics)|mention in bill of materials, datasheet|x|||
|||justification of selection |explanatory text document|||x|
|||list of dependencies (e.g. additional hardware/software)||||x|
|ASM-GEN|assembly (general)|involved components|bill of materials|x|||
|||geometry|CAD model, assembly drawing||x||
|||joining technology|defined in CAD model, specified in assembly drawing|x|||
|||junctions|defined in CAD model, specified in assembly drawing|x|||
|||assembly instructions|explanatory text document||x||
|||justification of technical design|explanatory text document, calculations|||x|
|||dismantling plan|explanatory text document|||x|
|ASM-MEC|mechanically joined assembly|preloads (e.g. tightening torque)|specified in assembly drawing||x||
|||calibration|calibration plan||x||
|||required special tools|list of special tools||x||
|||additional materials (e.g. lubricants)|list of additional materials||x||
|||justification of technical design|explanatory text document, calculations|||x|
|ASM-WEL|welded assembly|welding seam geometry (including welding seam preparation)|defined in CAD model, specified in assembly drawing, welding specification||x||
|||filler material|defined in CAD model, specified in assembly drawing, welding specification||x||
|||welding process|defined in CAD model, specified in assembly drawing, welding specification||x||
|||welding sequence|welding sequence plan||x||
|||justification of technical design|explanatory text document, calculations|||x|
|ASM-PCB|printed circuit board|involved components|bill of materials|x|||
|||interconnections between components|circuit diagram|x|||
|||pin assignment|board schematics/included in circuit diagram||x||
|||PCB layout|PCB overlay diagram|x|||
|||production specification|gerber file|x|||
|||justification of technical design|explanatory text document, calculations|||x|
|POST-COT|coating/filling|involved areas|defined in CAD model, specified in technical drawing, manufacturing instructions|x|||
|||coating/filling specifications|defined in CAD model, specified in technical drawing, manufacturing instructions|x|||
|||coating/filling technology|defined in CAD model, specified in technical drawing, manufacturing instructions||x||
|||additional (e.g. coating/filling) materials|defined in CAD model, specified in technical drawing, manufacturing instructions||x||
|||specialized tools|list of specialized tools||x||
|||justification of selection (e.g. of process, areas, materials)|explanatory text document, calculations|||x|
|POST-PRO|change of material properties|involved components/assemblies/areas|defined in CAD model, specified in technical drawing, manufacturing instructions|x|||
|||targeted material properties|defined in CAD model, specified in technical drawing, manufacturing instructions|x|||
|||processing technology|defined in CAD model, specified in technical drawing, manufacturing instructions||x||
|||additional materials|defined in CAD model, specified in technical drawing, manufacturing instructions||x||
|||specialized tools|list of specialized tools||x||
|||justification of selection (e.g. of process, areas, materials)|explanatory text document, calculations|||x|
|POST-IT|IT setup|unambiguous reference to the corresponding release|URL (to source code)|x|||
|||license|URL to license|x|||
|||flashing/installation instructions|explanatory text document||x||
|||dependencies/system requirements|list of dependencies/system requirements|||x|
|||justification of selection|explanatory text document|||x|
